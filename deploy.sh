#!/bin/sh
oc login https://astra5007.startdedicated.de:8443 -u $1 -p $2 --insecure-skip-tls-verify
oc project mike
oc delete -f ./production-deployment.yml
oc create -f ./production-deployment.yml