#!/bin/sh
docker login registry.gitlab.com -u $1 -p $2
docker build -t registry.gitlab.com/mk-ad/apples-and-robots .
docker push registry.gitlab.com/mk-ad/apples-and-robots